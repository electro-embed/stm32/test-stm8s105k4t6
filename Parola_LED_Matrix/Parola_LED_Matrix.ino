#include <MD_Parola.h>
#include <MD_MAX72xx.h>
#include <SPI.h>
#include <Arduino.h>

#define PIN_CLK PC5
#define PIN_CS PC4
#define PIN_DIN PC6

#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES 1

#define CLK_PIN   PIN_CLK
#define DATA_PIN  PIN_DIN
#define CS_PIN    PIN_CS

MD_Parola P = MD_Parola(HARDWARE_TYPE, CS_PIN, MAX_DEVICES);

void setup()
{
  P.begin();
}

void loop()
{
  if (P.displayAnimate()) {
    P.displayText("Hello", PA_CENTER, P.getSpeed(), P.getPause(), PA_SCROLL_DOWN, PA_SCROLL_UP);
  }
}
